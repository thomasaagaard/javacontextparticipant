package seacow;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * ContextSession Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.18.0)",
    comments = "Source: seacow.proto")
public final class ContextSessionGrpc {

  private ContextSessionGrpc() {}

  public static final String SERVICE_NAME = "seacow.ContextSession";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.CreateRequest,
      seacow.Seacow.CreateResponse> getCreateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Create",
      requestType = seacow.Seacow.CreateRequest.class,
      responseType = seacow.Seacow.CreateResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.CreateRequest,
      seacow.Seacow.CreateResponse> getCreateMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.CreateRequest, seacow.Seacow.CreateResponse> getCreateMethod;
    if ((getCreateMethod = ContextSessionGrpc.getCreateMethod) == null) {
      synchronized (ContextSessionGrpc.class) {
        if ((getCreateMethod = ContextSessionGrpc.getCreateMethod) == null) {
          ContextSessionGrpc.getCreateMethod = getCreateMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.CreateRequest, seacow.Seacow.CreateResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextSession", "Create"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.CreateRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.CreateResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextSessionMethodDescriptorSupplier("Create"))
                  .build();
          }
        }
     }
     return getCreateMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.ActivateRequest,
      seacow.Seacow.ActivateResponse> getActivateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Activate",
      requestType = seacow.Seacow.ActivateRequest.class,
      responseType = seacow.Seacow.ActivateResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.ActivateRequest,
      seacow.Seacow.ActivateResponse> getActivateMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.ActivateRequest, seacow.Seacow.ActivateResponse> getActivateMethod;
    if ((getActivateMethod = ContextSessionGrpc.getActivateMethod) == null) {
      synchronized (ContextSessionGrpc.class) {
        if ((getActivateMethod = ContextSessionGrpc.getActivateMethod) == null) {
          ContextSessionGrpc.getActivateMethod = getActivateMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.ActivateRequest, seacow.Seacow.ActivateResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextSession", "Activate"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ActivateRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ActivateResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextSessionMethodDescriptorSupplier("Activate"))
                  .build();
          }
        }
     }
     return getActivateMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ContextSessionStub newStub(io.grpc.Channel channel) {
    return new ContextSessionStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ContextSessionBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ContextSessionBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ContextSessionFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ContextSessionFutureStub(channel);
  }

  /**
   * <pre>
   * ContextSession Service
   * </pre>
   */
  public static abstract class ContextSessionImplBase implements io.grpc.BindableService {

    /**
     */
    public void create(seacow.Seacow.CreateRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.CreateResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateMethod(), responseObserver);
    }

    /**
     */
    public void activate(seacow.Seacow.ActivateRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ActivateResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getActivateMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCreateMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.CreateRequest,
                seacow.Seacow.CreateResponse>(
                  this, METHODID_CREATE)))
          .addMethod(
            getActivateMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.ActivateRequest,
                seacow.Seacow.ActivateResponse>(
                  this, METHODID_ACTIVATE)))
          .build();
    }
  }

  /**
   * <pre>
   * ContextSession Service
   * </pre>
   */
  public static final class ContextSessionStub extends io.grpc.stub.AbstractStub<ContextSessionStub> {
    private ContextSessionStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextSessionStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextSessionStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextSessionStub(channel, callOptions);
    }

    /**
     */
    public void create(seacow.Seacow.CreateRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.CreateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void activate(seacow.Seacow.ActivateRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ActivateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getActivateMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * ContextSession Service
   * </pre>
   */
  public static final class ContextSessionBlockingStub extends io.grpc.stub.AbstractStub<ContextSessionBlockingStub> {
    private ContextSessionBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextSessionBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextSessionBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextSessionBlockingStub(channel, callOptions);
    }

    /**
     */
    public seacow.Seacow.CreateResponse create(seacow.Seacow.CreateRequest request) {
      return blockingUnaryCall(
          getChannel(), getCreateMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.ActivateResponse activate(seacow.Seacow.ActivateRequest request) {
      return blockingUnaryCall(
          getChannel(), getActivateMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * ContextSession Service
   * </pre>
   */
  public static final class ContextSessionFutureStub extends io.grpc.stub.AbstractStub<ContextSessionFutureStub> {
    private ContextSessionFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextSessionFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextSessionFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextSessionFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.CreateResponse> create(
        seacow.Seacow.CreateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.ActivateResponse> activate(
        seacow.Seacow.ActivateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getActivateMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CREATE = 0;
  private static final int METHODID_ACTIVATE = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ContextSessionImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ContextSessionImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CREATE:
          serviceImpl.create((seacow.Seacow.CreateRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.CreateResponse>) responseObserver);
          break;
        case METHODID_ACTIVATE:
          serviceImpl.activate((seacow.Seacow.ActivateRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.ActivateResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ContextSessionBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ContextSessionBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return seacow.Seacow.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ContextSession");
    }
  }

  private static final class ContextSessionFileDescriptorSupplier
      extends ContextSessionBaseDescriptorSupplier {
    ContextSessionFileDescriptorSupplier() {}
  }

  private static final class ContextSessionMethodDescriptorSupplier
      extends ContextSessionBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ContextSessionMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ContextSessionGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ContextSessionFileDescriptorSupplier())
              .addMethod(getCreateMethod())
              .addMethod(getActivateMethod())
              .build();
        }
      }
    }
    return result;
  }
}
