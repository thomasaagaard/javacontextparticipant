package seacow;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * ContextFilter Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.18.0)",
    comments = "Source: seacow.proto")
public final class ContextFilterGrpc {

  private ContextFilterGrpc() {}

  public static final String SERVICE_NAME = "seacow.ContextFilter";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.SetSubjectsOfInterestRequest,
      seacow.Seacow.SetSubjectsOfInterestResponse> getSetSubjectsOfInterestMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetSubjectsOfInterest",
      requestType = seacow.Seacow.SetSubjectsOfInterestRequest.class,
      responseType = seacow.Seacow.SetSubjectsOfInterestResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.SetSubjectsOfInterestRequest,
      seacow.Seacow.SetSubjectsOfInterestResponse> getSetSubjectsOfInterestMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.SetSubjectsOfInterestRequest, seacow.Seacow.SetSubjectsOfInterestResponse> getSetSubjectsOfInterestMethod;
    if ((getSetSubjectsOfInterestMethod = ContextFilterGrpc.getSetSubjectsOfInterestMethod) == null) {
      synchronized (ContextFilterGrpc.class) {
        if ((getSetSubjectsOfInterestMethod = ContextFilterGrpc.getSetSubjectsOfInterestMethod) == null) {
          ContextFilterGrpc.getSetSubjectsOfInterestMethod = getSetSubjectsOfInterestMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.SetSubjectsOfInterestRequest, seacow.Seacow.SetSubjectsOfInterestResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextFilter", "SetSubjectsOfInterest"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SetSubjectsOfInterestRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SetSubjectsOfInterestResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextFilterMethodDescriptorSupplier("SetSubjectsOfInterest"))
                  .build();
          }
        }
     }
     return getSetSubjectsOfInterestMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.GetSubjectsOfInterestRequest,
      seacow.Seacow.GetSubjectsOfInterestResponse> getGetSubjectsOfInterestMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetSubjectsOfInterest",
      requestType = seacow.Seacow.GetSubjectsOfInterestRequest.class,
      responseType = seacow.Seacow.GetSubjectsOfInterestResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.GetSubjectsOfInterestRequest,
      seacow.Seacow.GetSubjectsOfInterestResponse> getGetSubjectsOfInterestMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.GetSubjectsOfInterestRequest, seacow.Seacow.GetSubjectsOfInterestResponse> getGetSubjectsOfInterestMethod;
    if ((getGetSubjectsOfInterestMethod = ContextFilterGrpc.getGetSubjectsOfInterestMethod) == null) {
      synchronized (ContextFilterGrpc.class) {
        if ((getGetSubjectsOfInterestMethod = ContextFilterGrpc.getGetSubjectsOfInterestMethod) == null) {
          ContextFilterGrpc.getGetSubjectsOfInterestMethod = getGetSubjectsOfInterestMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.GetSubjectsOfInterestRequest, seacow.Seacow.GetSubjectsOfInterestResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextFilter", "GetSubjectsOfInterest"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.GetSubjectsOfInterestRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.GetSubjectsOfInterestResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextFilterMethodDescriptorSupplier("GetSubjectsOfInterest"))
                  .build();
          }
        }
     }
     return getGetSubjectsOfInterestMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.ClearFilterRequest,
      seacow.Seacow.ClearFilterResponse> getClearFilterMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ClearFilter",
      requestType = seacow.Seacow.ClearFilterRequest.class,
      responseType = seacow.Seacow.ClearFilterResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.ClearFilterRequest,
      seacow.Seacow.ClearFilterResponse> getClearFilterMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.ClearFilterRequest, seacow.Seacow.ClearFilterResponse> getClearFilterMethod;
    if ((getClearFilterMethod = ContextFilterGrpc.getClearFilterMethod) == null) {
      synchronized (ContextFilterGrpc.class) {
        if ((getClearFilterMethod = ContextFilterGrpc.getClearFilterMethod) == null) {
          ContextFilterGrpc.getClearFilterMethod = getClearFilterMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.ClearFilterRequest, seacow.Seacow.ClearFilterResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextFilter", "ClearFilter"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ClearFilterRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ClearFilterResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextFilterMethodDescriptorSupplier("ClearFilter"))
                  .build();
          }
        }
     }
     return getClearFilterMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ContextFilterStub newStub(io.grpc.Channel channel) {
    return new ContextFilterStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ContextFilterBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ContextFilterBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ContextFilterFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ContextFilterFutureStub(channel);
  }

  /**
   * <pre>
   * ContextFilter Service
   * </pre>
   */
  public static abstract class ContextFilterImplBase implements io.grpc.BindableService {

    /**
     */
    public void setSubjectsOfInterest(seacow.Seacow.SetSubjectsOfInterestRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SetSubjectsOfInterestResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSetSubjectsOfInterestMethod(), responseObserver);
    }

    /**
     */
    public void getSubjectsOfInterest(seacow.Seacow.GetSubjectsOfInterestRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.GetSubjectsOfInterestResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetSubjectsOfInterestMethod(), responseObserver);
    }

    /**
     */
    public void clearFilter(seacow.Seacow.ClearFilterRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ClearFilterResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getClearFilterMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetSubjectsOfInterestMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.SetSubjectsOfInterestRequest,
                seacow.Seacow.SetSubjectsOfInterestResponse>(
                  this, METHODID_SET_SUBJECTS_OF_INTEREST)))
          .addMethod(
            getGetSubjectsOfInterestMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.GetSubjectsOfInterestRequest,
                seacow.Seacow.GetSubjectsOfInterestResponse>(
                  this, METHODID_GET_SUBJECTS_OF_INTEREST)))
          .addMethod(
            getClearFilterMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.ClearFilterRequest,
                seacow.Seacow.ClearFilterResponse>(
                  this, METHODID_CLEAR_FILTER)))
          .build();
    }
  }

  /**
   * <pre>
   * ContextFilter Service
   * </pre>
   */
  public static final class ContextFilterStub extends io.grpc.stub.AbstractStub<ContextFilterStub> {
    private ContextFilterStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextFilterStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextFilterStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextFilterStub(channel, callOptions);
    }

    /**
     */
    public void setSubjectsOfInterest(seacow.Seacow.SetSubjectsOfInterestRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SetSubjectsOfInterestResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetSubjectsOfInterestMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getSubjectsOfInterest(seacow.Seacow.GetSubjectsOfInterestRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.GetSubjectsOfInterestResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetSubjectsOfInterestMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void clearFilter(seacow.Seacow.ClearFilterRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ClearFilterResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getClearFilterMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * ContextFilter Service
   * </pre>
   */
  public static final class ContextFilterBlockingStub extends io.grpc.stub.AbstractStub<ContextFilterBlockingStub> {
    private ContextFilterBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextFilterBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextFilterBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextFilterBlockingStub(channel, callOptions);
    }

    /**
     */
    public seacow.Seacow.SetSubjectsOfInterestResponse setSubjectsOfInterest(seacow.Seacow.SetSubjectsOfInterestRequest request) {
      return blockingUnaryCall(
          getChannel(), getSetSubjectsOfInterestMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.GetSubjectsOfInterestResponse getSubjectsOfInterest(seacow.Seacow.GetSubjectsOfInterestRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetSubjectsOfInterestMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.ClearFilterResponse clearFilter(seacow.Seacow.ClearFilterRequest request) {
      return blockingUnaryCall(
          getChannel(), getClearFilterMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * ContextFilter Service
   * </pre>
   */
  public static final class ContextFilterFutureStub extends io.grpc.stub.AbstractStub<ContextFilterFutureStub> {
    private ContextFilterFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextFilterFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextFilterFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextFilterFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.SetSubjectsOfInterestResponse> setSubjectsOfInterest(
        seacow.Seacow.SetSubjectsOfInterestRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSetSubjectsOfInterestMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.GetSubjectsOfInterestResponse> getSubjectsOfInterest(
        seacow.Seacow.GetSubjectsOfInterestRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetSubjectsOfInterestMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.ClearFilterResponse> clearFilter(
        seacow.Seacow.ClearFilterRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getClearFilterMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_SUBJECTS_OF_INTEREST = 0;
  private static final int METHODID_GET_SUBJECTS_OF_INTEREST = 1;
  private static final int METHODID_CLEAR_FILTER = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ContextFilterImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ContextFilterImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_SUBJECTS_OF_INTEREST:
          serviceImpl.setSubjectsOfInterest((seacow.Seacow.SetSubjectsOfInterestRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.SetSubjectsOfInterestResponse>) responseObserver);
          break;
        case METHODID_GET_SUBJECTS_OF_INTEREST:
          serviceImpl.getSubjectsOfInterest((seacow.Seacow.GetSubjectsOfInterestRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.GetSubjectsOfInterestResponse>) responseObserver);
          break;
        case METHODID_CLEAR_FILTER:
          serviceImpl.clearFilter((seacow.Seacow.ClearFilterRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.ClearFilterResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ContextFilterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ContextFilterBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return seacow.Seacow.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ContextFilter");
    }
  }

  private static final class ContextFilterFileDescriptorSupplier
      extends ContextFilterBaseDescriptorSupplier {
    ContextFilterFileDescriptorSupplier() {}
  }

  private static final class ContextFilterMethodDescriptorSupplier
      extends ContextFilterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ContextFilterMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ContextFilterGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ContextFilterFileDescriptorSupplier())
              .addMethod(getSetSubjectsOfInterestMethod())
              .addMethod(getGetSubjectsOfInterestMethod())
              .addMethod(getClearFilterMethod())
              .build();
        }
      }
    }
    return result;
  }
}
