package seacow;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * Registry Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.18.0)",
    comments = "Source: seacow.proto")
public final class RegistryGrpc {

  private RegistryGrpc() {}

  public static final String SERVICE_NAME = "seacow.Registry";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.ComponentByIDRequest,
      seacow.Seacow.ComponentByIDResponse> getComponentByIDMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ComponentByID",
      requestType = seacow.Seacow.ComponentByIDRequest.class,
      responseType = seacow.Seacow.ComponentByIDResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.ComponentByIDRequest,
      seacow.Seacow.ComponentByIDResponse> getComponentByIDMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.ComponentByIDRequest, seacow.Seacow.ComponentByIDResponse> getComponentByIDMethod;
    if ((getComponentByIDMethod = RegistryGrpc.getComponentByIDMethod) == null) {
      synchronized (RegistryGrpc.class) {
        if ((getComponentByIDMethod = RegistryGrpc.getComponentByIDMethod) == null) {
          RegistryGrpc.getComponentByIDMethod = getComponentByIDMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.ComponentByIDRequest, seacow.Seacow.ComponentByIDResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.Registry", "ComponentByID"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ComponentByIDRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ComponentByIDResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new RegistryMethodDescriptorSupplier("ComponentByID"))
                  .build();
          }
        }
     }
     return getComponentByIDMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.LocateByTagRequest,
      seacow.Seacow.LocateByTagResponse> getLocateByTagMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "LocateByTag",
      requestType = seacow.Seacow.LocateByTagRequest.class,
      responseType = seacow.Seacow.LocateByTagResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.LocateByTagRequest,
      seacow.Seacow.LocateByTagResponse> getLocateByTagMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.LocateByTagRequest, seacow.Seacow.LocateByTagResponse> getLocateByTagMethod;
    if ((getLocateByTagMethod = RegistryGrpc.getLocateByTagMethod) == null) {
      synchronized (RegistryGrpc.class) {
        if ((getLocateByTagMethod = RegistryGrpc.getLocateByTagMethod) == null) {
          RegistryGrpc.getLocateByTagMethod = getLocateByTagMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.LocateByTagRequest, seacow.Seacow.LocateByTagResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.Registry", "LocateByTag"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.LocateByTagRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.LocateByTagResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new RegistryMethodDescriptorSupplier("LocateByTag"))
                  .build();
          }
        }
     }
     return getLocateByTagMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.OfTypeRequest,
      seacow.Seacow.OfTypeResponse> getOfTypeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "OfType",
      requestType = seacow.Seacow.OfTypeRequest.class,
      responseType = seacow.Seacow.OfTypeResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.OfTypeRequest,
      seacow.Seacow.OfTypeResponse> getOfTypeMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.OfTypeRequest, seacow.Seacow.OfTypeResponse> getOfTypeMethod;
    if ((getOfTypeMethod = RegistryGrpc.getOfTypeMethod) == null) {
      synchronized (RegistryGrpc.class) {
        if ((getOfTypeMethod = RegistryGrpc.getOfTypeMethod) == null) {
          RegistryGrpc.getOfTypeMethod = getOfTypeMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.OfTypeRequest, seacow.Seacow.OfTypeResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.Registry", "OfType"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.OfTypeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.OfTypeResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new RegistryMethodDescriptorSupplier("OfType"))
                  .build();
          }
        }
     }
     return getOfTypeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.LocalActionsRequest,
      seacow.Seacow.LocalActionsResponse> getLocalActionsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "LocalActions",
      requestType = seacow.Seacow.LocalActionsRequest.class,
      responseType = seacow.Seacow.LocalActionsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.LocalActionsRequest,
      seacow.Seacow.LocalActionsResponse> getLocalActionsMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.LocalActionsRequest, seacow.Seacow.LocalActionsResponse> getLocalActionsMethod;
    if ((getLocalActionsMethod = RegistryGrpc.getLocalActionsMethod) == null) {
      synchronized (RegistryGrpc.class) {
        if ((getLocalActionsMethod = RegistryGrpc.getLocalActionsMethod) == null) {
          RegistryGrpc.getLocalActionsMethod = getLocalActionsMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.LocalActionsRequest, seacow.Seacow.LocalActionsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.Registry", "LocalActions"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.LocalActionsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.LocalActionsResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new RegistryMethodDescriptorSupplier("LocalActions"))
                  .build();
          }
        }
     }
     return getLocalActionsMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static RegistryStub newStub(io.grpc.Channel channel) {
    return new RegistryStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static RegistryBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new RegistryBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static RegistryFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new RegistryFutureStub(channel);
  }

  /**
   * <pre>
   * Registry Service
   * </pre>
   */
  public static abstract class RegistryImplBase implements io.grpc.BindableService {

    /**
     */
    public void componentByID(seacow.Seacow.ComponentByIDRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ComponentByIDResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getComponentByIDMethod(), responseObserver);
    }

    /**
     */
    public void locateByTag(seacow.Seacow.LocateByTagRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.LocateByTagResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLocateByTagMethod(), responseObserver);
    }

    /**
     */
    public void ofType(seacow.Seacow.OfTypeRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.OfTypeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getOfTypeMethod(), responseObserver);
    }

    /**
     * <pre>
     * Note: this is a Manatee only operation
     * </pre>
     */
    public void localActions(seacow.Seacow.LocalActionsRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.LocalActionsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLocalActionsMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getComponentByIDMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.ComponentByIDRequest,
                seacow.Seacow.ComponentByIDResponse>(
                  this, METHODID_COMPONENT_BY_ID)))
          .addMethod(
            getLocateByTagMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.LocateByTagRequest,
                seacow.Seacow.LocateByTagResponse>(
                  this, METHODID_LOCATE_BY_TAG)))
          .addMethod(
            getOfTypeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.OfTypeRequest,
                seacow.Seacow.OfTypeResponse>(
                  this, METHODID_OF_TYPE)))
          .addMethod(
            getLocalActionsMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.LocalActionsRequest,
                seacow.Seacow.LocalActionsResponse>(
                  this, METHODID_LOCAL_ACTIONS)))
          .build();
    }
  }

  /**
   * <pre>
   * Registry Service
   * </pre>
   */
  public static final class RegistryStub extends io.grpc.stub.AbstractStub<RegistryStub> {
    private RegistryStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RegistryStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RegistryStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RegistryStub(channel, callOptions);
    }

    /**
     */
    public void componentByID(seacow.Seacow.ComponentByIDRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ComponentByIDResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getComponentByIDMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void locateByTag(seacow.Seacow.LocateByTagRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.LocateByTagResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLocateByTagMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void ofType(seacow.Seacow.OfTypeRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.OfTypeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getOfTypeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * Note: this is a Manatee only operation
     * </pre>
     */
    public void localActions(seacow.Seacow.LocalActionsRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.LocalActionsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLocalActionsMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * Registry Service
   * </pre>
   */
  public static final class RegistryBlockingStub extends io.grpc.stub.AbstractStub<RegistryBlockingStub> {
    private RegistryBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RegistryBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RegistryBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RegistryBlockingStub(channel, callOptions);
    }

    /**
     */
    public seacow.Seacow.ComponentByIDResponse componentByID(seacow.Seacow.ComponentByIDRequest request) {
      return blockingUnaryCall(
          getChannel(), getComponentByIDMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.LocateByTagResponse locateByTag(seacow.Seacow.LocateByTagRequest request) {
      return blockingUnaryCall(
          getChannel(), getLocateByTagMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.OfTypeResponse ofType(seacow.Seacow.OfTypeRequest request) {
      return blockingUnaryCall(
          getChannel(), getOfTypeMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * Note: this is a Manatee only operation
     * </pre>
     */
    public seacow.Seacow.LocalActionsResponse localActions(seacow.Seacow.LocalActionsRequest request) {
      return blockingUnaryCall(
          getChannel(), getLocalActionsMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * Registry Service
   * </pre>
   */
  public static final class RegistryFutureStub extends io.grpc.stub.AbstractStub<RegistryFutureStub> {
    private RegistryFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RegistryFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RegistryFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RegistryFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.ComponentByIDResponse> componentByID(
        seacow.Seacow.ComponentByIDRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getComponentByIDMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.LocateByTagResponse> locateByTag(
        seacow.Seacow.LocateByTagRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getLocateByTagMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.OfTypeResponse> ofType(
        seacow.Seacow.OfTypeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getOfTypeMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * Note: this is a Manatee only operation
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.LocalActionsResponse> localActions(
        seacow.Seacow.LocalActionsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getLocalActionsMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_COMPONENT_BY_ID = 0;
  private static final int METHODID_LOCATE_BY_TAG = 1;
  private static final int METHODID_OF_TYPE = 2;
  private static final int METHODID_LOCAL_ACTIONS = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final RegistryImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(RegistryImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_COMPONENT_BY_ID:
          serviceImpl.componentByID((seacow.Seacow.ComponentByIDRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.ComponentByIDResponse>) responseObserver);
          break;
        case METHODID_LOCATE_BY_TAG:
          serviceImpl.locateByTag((seacow.Seacow.LocateByTagRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.LocateByTagResponse>) responseObserver);
          break;
        case METHODID_OF_TYPE:
          serviceImpl.ofType((seacow.Seacow.OfTypeRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.OfTypeResponse>) responseObserver);
          break;
        case METHODID_LOCAL_ACTIONS:
          serviceImpl.localActions((seacow.Seacow.LocalActionsRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.LocalActionsResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class RegistryBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    RegistryBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return seacow.Seacow.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Registry");
    }
  }

  private static final class RegistryFileDescriptorSupplier
      extends RegistryBaseDescriptorSupplier {
    RegistryFileDescriptorSupplier() {}
  }

  private static final class RegistryMethodDescriptorSupplier
      extends RegistryBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    RegistryMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (RegistryGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new RegistryFileDescriptorSupplier())
              .addMethod(getComponentByIDMethod())
              .addMethod(getLocateByTagMethod())
              .addMethod(getOfTypeMethod())
              .addMethod(getLocalActionsMethod())
              .build();
        }
      }
    }
    return result;
  }
}
