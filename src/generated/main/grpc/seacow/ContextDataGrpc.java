package seacow;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * ContextData Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.18.0)",
    comments = "Source: seacow.proto")
public final class ContextDataGrpc {

  private ContextDataGrpc() {}

  public static final String SERVICE_NAME = "seacow.ContextData";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.GetItemNamesRequest,
      seacow.Seacow.GetItemNamesResponse> getGetItemNamesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetItemNames",
      requestType = seacow.Seacow.GetItemNamesRequest.class,
      responseType = seacow.Seacow.GetItemNamesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.GetItemNamesRequest,
      seacow.Seacow.GetItemNamesResponse> getGetItemNamesMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.GetItemNamesRequest, seacow.Seacow.GetItemNamesResponse> getGetItemNamesMethod;
    if ((getGetItemNamesMethod = ContextDataGrpc.getGetItemNamesMethod) == null) {
      synchronized (ContextDataGrpc.class) {
        if ((getGetItemNamesMethod = ContextDataGrpc.getGetItemNamesMethod) == null) {
          ContextDataGrpc.getGetItemNamesMethod = getGetItemNamesMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.GetItemNamesRequest, seacow.Seacow.GetItemNamesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextData", "GetItemNames"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.GetItemNamesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.GetItemNamesResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextDataMethodDescriptorSupplier("GetItemNames"))
                  .build();
          }
        }
     }
     return getGetItemNamesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.DeleteItemsRequest,
      seacow.Seacow.DeleteItemsResponse> getDeleteItemsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DeleteItems",
      requestType = seacow.Seacow.DeleteItemsRequest.class,
      responseType = seacow.Seacow.DeleteItemsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.DeleteItemsRequest,
      seacow.Seacow.DeleteItemsResponse> getDeleteItemsMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.DeleteItemsRequest, seacow.Seacow.DeleteItemsResponse> getDeleteItemsMethod;
    if ((getDeleteItemsMethod = ContextDataGrpc.getDeleteItemsMethod) == null) {
      synchronized (ContextDataGrpc.class) {
        if ((getDeleteItemsMethod = ContextDataGrpc.getDeleteItemsMethod) == null) {
          ContextDataGrpc.getDeleteItemsMethod = getDeleteItemsMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.DeleteItemsRequest, seacow.Seacow.DeleteItemsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextData", "DeleteItems"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.DeleteItemsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.DeleteItemsResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextDataMethodDescriptorSupplier("DeleteItems"))
                  .build();
          }
        }
     }
     return getDeleteItemsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.SetItemValuesRequest,
      seacow.Seacow.SetItemValuesResponse> getSetItemValuesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetItemValues",
      requestType = seacow.Seacow.SetItemValuesRequest.class,
      responseType = seacow.Seacow.SetItemValuesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.SetItemValuesRequest,
      seacow.Seacow.SetItemValuesResponse> getSetItemValuesMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.SetItemValuesRequest, seacow.Seacow.SetItemValuesResponse> getSetItemValuesMethod;
    if ((getSetItemValuesMethod = ContextDataGrpc.getSetItemValuesMethod) == null) {
      synchronized (ContextDataGrpc.class) {
        if ((getSetItemValuesMethod = ContextDataGrpc.getSetItemValuesMethod) == null) {
          ContextDataGrpc.getSetItemValuesMethod = getSetItemValuesMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.SetItemValuesRequest, seacow.Seacow.SetItemValuesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextData", "SetItemValues"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SetItemValuesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SetItemValuesResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextDataMethodDescriptorSupplier("SetItemValues"))
                  .build();
          }
        }
     }
     return getSetItemValuesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.GetItemValuesRequest,
      seacow.Seacow.GetItemValuesResponse> getGetItemValuesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetItemValues",
      requestType = seacow.Seacow.GetItemValuesRequest.class,
      responseType = seacow.Seacow.GetItemValuesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.GetItemValuesRequest,
      seacow.Seacow.GetItemValuesResponse> getGetItemValuesMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.GetItemValuesRequest, seacow.Seacow.GetItemValuesResponse> getGetItemValuesMethod;
    if ((getGetItemValuesMethod = ContextDataGrpc.getGetItemValuesMethod) == null) {
      synchronized (ContextDataGrpc.class) {
        if ((getGetItemValuesMethod = ContextDataGrpc.getGetItemValuesMethod) == null) {
          ContextDataGrpc.getGetItemValuesMethod = getGetItemValuesMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.GetItemValuesRequest, seacow.Seacow.GetItemValuesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextData", "GetItemValues"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.GetItemValuesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.GetItemValuesResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextDataMethodDescriptorSupplier("GetItemValues"))
                  .build();
          }
        }
     }
     return getGetItemValuesMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ContextDataStub newStub(io.grpc.Channel channel) {
    return new ContextDataStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ContextDataBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ContextDataBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ContextDataFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ContextDataFutureStub(channel);
  }

  /**
   * <pre>
   * ContextData Service
   * </pre>
   */
  public static abstract class ContextDataImplBase implements io.grpc.BindableService {

    /**
     */
    public void getItemNames(seacow.Seacow.GetItemNamesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.GetItemNamesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetItemNamesMethod(), responseObserver);
    }

    /**
     */
    public void deleteItems(seacow.Seacow.DeleteItemsRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.DeleteItemsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteItemsMethod(), responseObserver);
    }

    /**
     */
    public void setItemValues(seacow.Seacow.SetItemValuesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SetItemValuesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSetItemValuesMethod(), responseObserver);
    }

    /**
     */
    public void getItemValues(seacow.Seacow.GetItemValuesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.GetItemValuesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetItemValuesMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetItemNamesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.GetItemNamesRequest,
                seacow.Seacow.GetItemNamesResponse>(
                  this, METHODID_GET_ITEM_NAMES)))
          .addMethod(
            getDeleteItemsMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.DeleteItemsRequest,
                seacow.Seacow.DeleteItemsResponse>(
                  this, METHODID_DELETE_ITEMS)))
          .addMethod(
            getSetItemValuesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.SetItemValuesRequest,
                seacow.Seacow.SetItemValuesResponse>(
                  this, METHODID_SET_ITEM_VALUES)))
          .addMethod(
            getGetItemValuesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.GetItemValuesRequest,
                seacow.Seacow.GetItemValuesResponse>(
                  this, METHODID_GET_ITEM_VALUES)))
          .build();
    }
  }

  /**
   * <pre>
   * ContextData Service
   * </pre>
   */
  public static final class ContextDataStub extends io.grpc.stub.AbstractStub<ContextDataStub> {
    private ContextDataStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextDataStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextDataStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextDataStub(channel, callOptions);
    }

    /**
     */
    public void getItemNames(seacow.Seacow.GetItemNamesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.GetItemNamesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetItemNamesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteItems(seacow.Seacow.DeleteItemsRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.DeleteItemsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteItemsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setItemValues(seacow.Seacow.SetItemValuesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SetItemValuesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetItemValuesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getItemValues(seacow.Seacow.GetItemValuesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.GetItemValuesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetItemValuesMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * ContextData Service
   * </pre>
   */
  public static final class ContextDataBlockingStub extends io.grpc.stub.AbstractStub<ContextDataBlockingStub> {
    private ContextDataBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextDataBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextDataBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextDataBlockingStub(channel, callOptions);
    }

    /**
     */
    public seacow.Seacow.GetItemNamesResponse getItemNames(seacow.Seacow.GetItemNamesRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetItemNamesMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.DeleteItemsResponse deleteItems(seacow.Seacow.DeleteItemsRequest request) {
      return blockingUnaryCall(
          getChannel(), getDeleteItemsMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.SetItemValuesResponse setItemValues(seacow.Seacow.SetItemValuesRequest request) {
      return blockingUnaryCall(
          getChannel(), getSetItemValuesMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.GetItemValuesResponse getItemValues(seacow.Seacow.GetItemValuesRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetItemValuesMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * ContextData Service
   * </pre>
   */
  public static final class ContextDataFutureStub extends io.grpc.stub.AbstractStub<ContextDataFutureStub> {
    private ContextDataFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextDataFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextDataFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextDataFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.GetItemNamesResponse> getItemNames(
        seacow.Seacow.GetItemNamesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetItemNamesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.DeleteItemsResponse> deleteItems(
        seacow.Seacow.DeleteItemsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteItemsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.SetItemValuesResponse> setItemValues(
        seacow.Seacow.SetItemValuesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSetItemValuesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.GetItemValuesResponse> getItemValues(
        seacow.Seacow.GetItemValuesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetItemValuesMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ITEM_NAMES = 0;
  private static final int METHODID_DELETE_ITEMS = 1;
  private static final int METHODID_SET_ITEM_VALUES = 2;
  private static final int METHODID_GET_ITEM_VALUES = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ContextDataImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ContextDataImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ITEM_NAMES:
          serviceImpl.getItemNames((seacow.Seacow.GetItemNamesRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.GetItemNamesResponse>) responseObserver);
          break;
        case METHODID_DELETE_ITEMS:
          serviceImpl.deleteItems((seacow.Seacow.DeleteItemsRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.DeleteItemsResponse>) responseObserver);
          break;
        case METHODID_SET_ITEM_VALUES:
          serviceImpl.setItemValues((seacow.Seacow.SetItemValuesRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.SetItemValuesResponse>) responseObserver);
          break;
        case METHODID_GET_ITEM_VALUES:
          serviceImpl.getItemValues((seacow.Seacow.GetItemValuesRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.GetItemValuesResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ContextDataBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ContextDataBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return seacow.Seacow.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ContextData");
    }
  }

  private static final class ContextDataFileDescriptorSupplier
      extends ContextDataBaseDescriptorSupplier {
    ContextDataFileDescriptorSupplier() {}
  }

  private static final class ContextDataMethodDescriptorSupplier
      extends ContextDataBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ContextDataMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ContextDataGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ContextDataFileDescriptorSupplier())
              .addMethod(getGetItemNamesMethod())
              .addMethod(getDeleteItemsMethod())
              .addMethod(getSetItemValuesMethod())
              .addMethod(getGetItemValuesMethod())
              .build();
        }
      }
    }
    return result;
  }
}
