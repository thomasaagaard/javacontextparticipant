package seacow;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * ContextManager Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.18.0)",
    comments = "Source: seacow.proto")
public final class ContextManagerGrpc {

  private ContextManagerGrpc() {}

  public static final String SERVICE_NAME = "seacow.ContextManager";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.SetActiveRequest,
      seacow.Seacow.SetActiveResponse> getSetActiveMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetActive",
      requestType = seacow.Seacow.SetActiveRequest.class,
      responseType = seacow.Seacow.SetActiveResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.SetActiveRequest,
      seacow.Seacow.SetActiveResponse> getSetActiveMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.SetActiveRequest, seacow.Seacow.SetActiveResponse> getSetActiveMethod;
    if ((getSetActiveMethod = ContextManagerGrpc.getSetActiveMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getSetActiveMethod = ContextManagerGrpc.getSetActiveMethod) == null) {
          ContextManagerGrpc.getSetActiveMethod = getSetActiveMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.SetActiveRequest, seacow.Seacow.SetActiveResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "SetActive"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SetActiveRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SetActiveResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("SetActive"))
                  .build();
          }
        }
     }
     return getSetActiveMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.JoinCommonContextRequest,
      seacow.Seacow.JoinCommonContextResponse> getJoinCommonContextMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "JoinCommonContext",
      requestType = seacow.Seacow.JoinCommonContextRequest.class,
      responseType = seacow.Seacow.JoinCommonContextResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.JoinCommonContextRequest,
      seacow.Seacow.JoinCommonContextResponse> getJoinCommonContextMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.JoinCommonContextRequest, seacow.Seacow.JoinCommonContextResponse> getJoinCommonContextMethod;
    if ((getJoinCommonContextMethod = ContextManagerGrpc.getJoinCommonContextMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getJoinCommonContextMethod = ContextManagerGrpc.getJoinCommonContextMethod) == null) {
          ContextManagerGrpc.getJoinCommonContextMethod = getJoinCommonContextMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.JoinCommonContextRequest, seacow.Seacow.JoinCommonContextResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "JoinCommonContext"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.JoinCommonContextRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.JoinCommonContextResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("JoinCommonContext"))
                  .build();
          }
        }
     }
     return getJoinCommonContextMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.LeaveCommonContextRequest,
      seacow.Seacow.LeaveCommonContextResponse> getLeaveCommonContextMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "LeaveCommonContext",
      requestType = seacow.Seacow.LeaveCommonContextRequest.class,
      responseType = seacow.Seacow.LeaveCommonContextResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.LeaveCommonContextRequest,
      seacow.Seacow.LeaveCommonContextResponse> getLeaveCommonContextMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.LeaveCommonContextRequest, seacow.Seacow.LeaveCommonContextResponse> getLeaveCommonContextMethod;
    if ((getLeaveCommonContextMethod = ContextManagerGrpc.getLeaveCommonContextMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getLeaveCommonContextMethod = ContextManagerGrpc.getLeaveCommonContextMethod) == null) {
          ContextManagerGrpc.getLeaveCommonContextMethod = getLeaveCommonContextMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.LeaveCommonContextRequest, seacow.Seacow.LeaveCommonContextResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "LeaveCommonContext"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.LeaveCommonContextRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.LeaveCommonContextResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("LeaveCommonContext"))
                  .build();
          }
        }
     }
     return getLeaveCommonContextMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.StartContextChangesRequest,
      seacow.Seacow.StartContextChangesResponse> getStartContextChangesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "StartContextChanges",
      requestType = seacow.Seacow.StartContextChangesRequest.class,
      responseType = seacow.Seacow.StartContextChangesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.StartContextChangesRequest,
      seacow.Seacow.StartContextChangesResponse> getStartContextChangesMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.StartContextChangesRequest, seacow.Seacow.StartContextChangesResponse> getStartContextChangesMethod;
    if ((getStartContextChangesMethod = ContextManagerGrpc.getStartContextChangesMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getStartContextChangesMethod = ContextManagerGrpc.getStartContextChangesMethod) == null) {
          ContextManagerGrpc.getStartContextChangesMethod = getStartContextChangesMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.StartContextChangesRequest, seacow.Seacow.StartContextChangesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "StartContextChanges"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.StartContextChangesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.StartContextChangesResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("StartContextChanges"))
                  .build();
          }
        }
     }
     return getStartContextChangesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.EndContextChangesRequest,
      seacow.Seacow.EndContextChangesResponse> getEndContextChangesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "EndContextChanges",
      requestType = seacow.Seacow.EndContextChangesRequest.class,
      responseType = seacow.Seacow.EndContextChangesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.EndContextChangesRequest,
      seacow.Seacow.EndContextChangesResponse> getEndContextChangesMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.EndContextChangesRequest, seacow.Seacow.EndContextChangesResponse> getEndContextChangesMethod;
    if ((getEndContextChangesMethod = ContextManagerGrpc.getEndContextChangesMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getEndContextChangesMethod = ContextManagerGrpc.getEndContextChangesMethod) == null) {
          ContextManagerGrpc.getEndContextChangesMethod = getEndContextChangesMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.EndContextChangesRequest, seacow.Seacow.EndContextChangesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "EndContextChanges"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.EndContextChangesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.EndContextChangesResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("EndContextChanges"))
                  .build();
          }
        }
     }
     return getEndContextChangesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.UndoContextChangesRequest,
      seacow.Seacow.UndoContextChangesResponse> getUndoContextChangesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UndoContextChanges",
      requestType = seacow.Seacow.UndoContextChangesRequest.class,
      responseType = seacow.Seacow.UndoContextChangesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.UndoContextChangesRequest,
      seacow.Seacow.UndoContextChangesResponse> getUndoContextChangesMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.UndoContextChangesRequest, seacow.Seacow.UndoContextChangesResponse> getUndoContextChangesMethod;
    if ((getUndoContextChangesMethod = ContextManagerGrpc.getUndoContextChangesMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getUndoContextChangesMethod = ContextManagerGrpc.getUndoContextChangesMethod) == null) {
          ContextManagerGrpc.getUndoContextChangesMethod = getUndoContextChangesMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.UndoContextChangesRequest, seacow.Seacow.UndoContextChangesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "UndoContextChanges"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.UndoContextChangesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.UndoContextChangesResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("UndoContextChanges"))
                  .build();
          }
        }
     }
     return getUndoContextChangesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.PublishChangesDecisionRequest,
      seacow.Seacow.PublishChangesDecisionResponse> getPublishChangesDecisionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "PublishChangesDecision",
      requestType = seacow.Seacow.PublishChangesDecisionRequest.class,
      responseType = seacow.Seacow.PublishChangesDecisionResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.PublishChangesDecisionRequest,
      seacow.Seacow.PublishChangesDecisionResponse> getPublishChangesDecisionMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.PublishChangesDecisionRequest, seacow.Seacow.PublishChangesDecisionResponse> getPublishChangesDecisionMethod;
    if ((getPublishChangesDecisionMethod = ContextManagerGrpc.getPublishChangesDecisionMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getPublishChangesDecisionMethod = ContextManagerGrpc.getPublishChangesDecisionMethod) == null) {
          ContextManagerGrpc.getPublishChangesDecisionMethod = getPublishChangesDecisionMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.PublishChangesDecisionRequest, seacow.Seacow.PublishChangesDecisionResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "PublishChangesDecision"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.PublishChangesDecisionRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.PublishChangesDecisionResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("PublishChangesDecision"))
                  .build();
          }
        }
     }
     return getPublishChangesDecisionMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.SuspendParticipationRequest,
      seacow.Seacow.SuspendParticipationResponse> getSuspendParticipationMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SuspendParticipation",
      requestType = seacow.Seacow.SuspendParticipationRequest.class,
      responseType = seacow.Seacow.SuspendParticipationResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.SuspendParticipationRequest,
      seacow.Seacow.SuspendParticipationResponse> getSuspendParticipationMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.SuspendParticipationRequest, seacow.Seacow.SuspendParticipationResponse> getSuspendParticipationMethod;
    if ((getSuspendParticipationMethod = ContextManagerGrpc.getSuspendParticipationMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getSuspendParticipationMethod = ContextManagerGrpc.getSuspendParticipationMethod) == null) {
          ContextManagerGrpc.getSuspendParticipationMethod = getSuspendParticipationMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.SuspendParticipationRequest, seacow.Seacow.SuspendParticipationResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "SuspendParticipation"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SuspendParticipationRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.SuspendParticipationResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("SuspendParticipation"))
                  .build();
          }
        }
     }
     return getSuspendParticipationMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.ResumeParticipationRequest,
      seacow.Seacow.ResumeParticipationResponse> getResumeParticipationMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ResumeParticipation",
      requestType = seacow.Seacow.ResumeParticipationRequest.class,
      responseType = seacow.Seacow.ResumeParticipationResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.ResumeParticipationRequest,
      seacow.Seacow.ResumeParticipationResponse> getResumeParticipationMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.ResumeParticipationRequest, seacow.Seacow.ResumeParticipationResponse> getResumeParticipationMethod;
    if ((getResumeParticipationMethod = ContextManagerGrpc.getResumeParticipationMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getResumeParticipationMethod = ContextManagerGrpc.getResumeParticipationMethod) == null) {
          ContextManagerGrpc.getResumeParticipationMethod = getResumeParticipationMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.ResumeParticipationRequest, seacow.Seacow.ResumeParticipationResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "ResumeParticipation"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ResumeParticipationRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ResumeParticipationResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("ResumeParticipation"))
                  .build();
          }
        }
     }
     return getResumeParticipationMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.MostRecentContextCouponRequest,
      seacow.Seacow.MostRecentContextCouponResponse> getMostRecentContextCouponMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "MostRecentContextCoupon",
      requestType = seacow.Seacow.MostRecentContextCouponRequest.class,
      responseType = seacow.Seacow.MostRecentContextCouponResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.MostRecentContextCouponRequest,
      seacow.Seacow.MostRecentContextCouponResponse> getMostRecentContextCouponMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.MostRecentContextCouponRequest, seacow.Seacow.MostRecentContextCouponResponse> getMostRecentContextCouponMethod;
    if ((getMostRecentContextCouponMethod = ContextManagerGrpc.getMostRecentContextCouponMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getMostRecentContextCouponMethod = ContextManagerGrpc.getMostRecentContextCouponMethod) == null) {
          ContextManagerGrpc.getMostRecentContextCouponMethod = getMostRecentContextCouponMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.MostRecentContextCouponRequest, seacow.Seacow.MostRecentContextCouponResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "MostRecentContextCoupon"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.MostRecentContextCouponRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.MostRecentContextCouponResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("MostRecentContextCoupon"))
                  .build();
          }
        }
     }
     return getMostRecentContextCouponMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.InstanceInformationRequest,
      seacow.Seacow.InstanceInformationResponse> getInstanceInformationMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "InstanceInformation",
      requestType = seacow.Seacow.InstanceInformationRequest.class,
      responseType = seacow.Seacow.InstanceInformationResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.InstanceInformationRequest,
      seacow.Seacow.InstanceInformationResponse> getInstanceInformationMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.InstanceInformationRequest, seacow.Seacow.InstanceInformationResponse> getInstanceInformationMethod;
    if ((getInstanceInformationMethod = ContextManagerGrpc.getInstanceInformationMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getInstanceInformationMethod = ContextManagerGrpc.getInstanceInformationMethod) == null) {
          ContextManagerGrpc.getInstanceInformationMethod = getInstanceInformationMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.InstanceInformationRequest, seacow.Seacow.InstanceInformationResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "InstanceInformation"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.InstanceInformationRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.InstanceInformationResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("InstanceInformation"))
                  .build();
          }
        }
     }
     return getInstanceInformationMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.ImplementationInformationRequest,
      seacow.Seacow.ImplementationInformationResponse> getImplementationInformationMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ImplementationInformation",
      requestType = seacow.Seacow.ImplementationInformationRequest.class,
      responseType = seacow.Seacow.ImplementationInformationResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.ImplementationInformationRequest,
      seacow.Seacow.ImplementationInformationResponse> getImplementationInformationMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.ImplementationInformationRequest, seacow.Seacow.ImplementationInformationResponse> getImplementationInformationMethod;
    if ((getImplementationInformationMethod = ContextManagerGrpc.getImplementationInformationMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getImplementationInformationMethod = ContextManagerGrpc.getImplementationInformationMethod) == null) {
          ContextManagerGrpc.getImplementationInformationMethod = getImplementationInformationMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.ImplementationInformationRequest, seacow.Seacow.ImplementationInformationResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "ImplementationInformation"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ImplementationInformationRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ImplementationInformationResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("ImplementationInformation"))
                  .build();
          }
        }
     }
     return getImplementationInformationMethod;
  }

  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.ContextParticipantStreamRequest,
      seacow.Seacow.ContextParticipantStreamResponse> getContextParticipantStreamMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ContextParticipantStream",
      requestType = seacow.Seacow.ContextParticipantStreamRequest.class,
      responseType = seacow.Seacow.ContextParticipantStreamResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<seacow.Seacow.ContextParticipantStreamRequest,
      seacow.Seacow.ContextParticipantStreamResponse> getContextParticipantStreamMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.ContextParticipantStreamRequest, seacow.Seacow.ContextParticipantStreamResponse> getContextParticipantStreamMethod;
    if ((getContextParticipantStreamMethod = ContextManagerGrpc.getContextParticipantStreamMethod) == null) {
      synchronized (ContextManagerGrpc.class) {
        if ((getContextParticipantStreamMethod = ContextManagerGrpc.getContextParticipantStreamMethod) == null) {
          ContextManagerGrpc.getContextParticipantStreamMethod = getContextParticipantStreamMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.ContextParticipantStreamRequest, seacow.Seacow.ContextParticipantStreamResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextManager", "ContextParticipantStream"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ContextParticipantStreamRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.ContextParticipantStreamResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextManagerMethodDescriptorSupplier("ContextParticipantStream"))
                  .build();
          }
        }
     }
     return getContextParticipantStreamMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ContextManagerStub newStub(io.grpc.Channel channel) {
    return new ContextManagerStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ContextManagerBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ContextManagerBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ContextManagerFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ContextManagerFutureStub(channel);
  }

  /**
   * <pre>
   * ContextManager Service
   * </pre>
   */
  public static abstract class ContextManagerImplBase implements io.grpc.BindableService {

    /**
     */
    public void setActive(seacow.Seacow.SetActiveRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SetActiveResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSetActiveMethod(), responseObserver);
    }

    /**
     */
    public void joinCommonContext(seacow.Seacow.JoinCommonContextRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.JoinCommonContextResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getJoinCommonContextMethod(), responseObserver);
    }

    /**
     */
    public void leaveCommonContext(seacow.Seacow.LeaveCommonContextRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.LeaveCommonContextResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLeaveCommonContextMethod(), responseObserver);
    }

    /**
     */
    public void startContextChanges(seacow.Seacow.StartContextChangesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.StartContextChangesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getStartContextChangesMethod(), responseObserver);
    }

    /**
     */
    public void endContextChanges(seacow.Seacow.EndContextChangesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.EndContextChangesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getEndContextChangesMethod(), responseObserver);
    }

    /**
     */
    public void undoContextChanges(seacow.Seacow.UndoContextChangesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.UndoContextChangesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUndoContextChangesMethod(), responseObserver);
    }

    /**
     */
    public void publishChangesDecision(seacow.Seacow.PublishChangesDecisionRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.PublishChangesDecisionResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getPublishChangesDecisionMethod(), responseObserver);
    }

    /**
     */
    public void suspendParticipation(seacow.Seacow.SuspendParticipationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SuspendParticipationResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSuspendParticipationMethod(), responseObserver);
    }

    /**
     */
    public void resumeParticipation(seacow.Seacow.ResumeParticipationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ResumeParticipationResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getResumeParticipationMethod(), responseObserver);
    }

    /**
     */
    public void mostRecentContextCoupon(seacow.Seacow.MostRecentContextCouponRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.MostRecentContextCouponResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getMostRecentContextCouponMethod(), responseObserver);
    }

    /**
     */
    public void instanceInformation(seacow.Seacow.InstanceInformationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.InstanceInformationResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getInstanceInformationMethod(), responseObserver);
    }

    /**
     */
    public void implementationInformation(seacow.Seacow.ImplementationInformationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ImplementationInformationResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getImplementationInformationMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<seacow.Seacow.ContextParticipantStreamRequest> contextParticipantStream(
        io.grpc.stub.StreamObserver<seacow.Seacow.ContextParticipantStreamResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(getContextParticipantStreamMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetActiveMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.SetActiveRequest,
                seacow.Seacow.SetActiveResponse>(
                  this, METHODID_SET_ACTIVE)))
          .addMethod(
            getJoinCommonContextMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.JoinCommonContextRequest,
                seacow.Seacow.JoinCommonContextResponse>(
                  this, METHODID_JOIN_COMMON_CONTEXT)))
          .addMethod(
            getLeaveCommonContextMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.LeaveCommonContextRequest,
                seacow.Seacow.LeaveCommonContextResponse>(
                  this, METHODID_LEAVE_COMMON_CONTEXT)))
          .addMethod(
            getStartContextChangesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.StartContextChangesRequest,
                seacow.Seacow.StartContextChangesResponse>(
                  this, METHODID_START_CONTEXT_CHANGES)))
          .addMethod(
            getEndContextChangesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.EndContextChangesRequest,
                seacow.Seacow.EndContextChangesResponse>(
                  this, METHODID_END_CONTEXT_CHANGES)))
          .addMethod(
            getUndoContextChangesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.UndoContextChangesRequest,
                seacow.Seacow.UndoContextChangesResponse>(
                  this, METHODID_UNDO_CONTEXT_CHANGES)))
          .addMethod(
            getPublishChangesDecisionMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.PublishChangesDecisionRequest,
                seacow.Seacow.PublishChangesDecisionResponse>(
                  this, METHODID_PUBLISH_CHANGES_DECISION)))
          .addMethod(
            getSuspendParticipationMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.SuspendParticipationRequest,
                seacow.Seacow.SuspendParticipationResponse>(
                  this, METHODID_SUSPEND_PARTICIPATION)))
          .addMethod(
            getResumeParticipationMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.ResumeParticipationRequest,
                seacow.Seacow.ResumeParticipationResponse>(
                  this, METHODID_RESUME_PARTICIPATION)))
          .addMethod(
            getMostRecentContextCouponMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.MostRecentContextCouponRequest,
                seacow.Seacow.MostRecentContextCouponResponse>(
                  this, METHODID_MOST_RECENT_CONTEXT_COUPON)))
          .addMethod(
            getInstanceInformationMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.InstanceInformationRequest,
                seacow.Seacow.InstanceInformationResponse>(
                  this, METHODID_INSTANCE_INFORMATION)))
          .addMethod(
            getImplementationInformationMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.ImplementationInformationRequest,
                seacow.Seacow.ImplementationInformationResponse>(
                  this, METHODID_IMPLEMENTATION_INFORMATION)))
          .addMethod(
            getContextParticipantStreamMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                seacow.Seacow.ContextParticipantStreamRequest,
                seacow.Seacow.ContextParticipantStreamResponse>(
                  this, METHODID_CONTEXT_PARTICIPANT_STREAM)))
          .build();
    }
  }

  /**
   * <pre>
   * ContextManager Service
   * </pre>
   */
  public static final class ContextManagerStub extends io.grpc.stub.AbstractStub<ContextManagerStub> {
    private ContextManagerStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextManagerStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextManagerStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextManagerStub(channel, callOptions);
    }

    /**
     */
    public void setActive(seacow.Seacow.SetActiveRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SetActiveResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetActiveMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void joinCommonContext(seacow.Seacow.JoinCommonContextRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.JoinCommonContextResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getJoinCommonContextMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void leaveCommonContext(seacow.Seacow.LeaveCommonContextRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.LeaveCommonContextResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLeaveCommonContextMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void startContextChanges(seacow.Seacow.StartContextChangesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.StartContextChangesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getStartContextChangesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void endContextChanges(seacow.Seacow.EndContextChangesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.EndContextChangesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getEndContextChangesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void undoContextChanges(seacow.Seacow.UndoContextChangesRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.UndoContextChangesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUndoContextChangesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void publishChangesDecision(seacow.Seacow.PublishChangesDecisionRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.PublishChangesDecisionResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getPublishChangesDecisionMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void suspendParticipation(seacow.Seacow.SuspendParticipationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.SuspendParticipationResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSuspendParticipationMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void resumeParticipation(seacow.Seacow.ResumeParticipationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ResumeParticipationResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getResumeParticipationMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void mostRecentContextCoupon(seacow.Seacow.MostRecentContextCouponRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.MostRecentContextCouponResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getMostRecentContextCouponMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void instanceInformation(seacow.Seacow.InstanceInformationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.InstanceInformationResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getInstanceInformationMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void implementationInformation(seacow.Seacow.ImplementationInformationRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.ImplementationInformationResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getImplementationInformationMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<seacow.Seacow.ContextParticipantStreamRequest> contextParticipantStream(
        io.grpc.stub.StreamObserver<seacow.Seacow.ContextParticipantStreamResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getContextParticipantStreamMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * ContextManager Service
   * </pre>
   */
  public static final class ContextManagerBlockingStub extends io.grpc.stub.AbstractStub<ContextManagerBlockingStub> {
    private ContextManagerBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextManagerBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextManagerBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextManagerBlockingStub(channel, callOptions);
    }

    /**
     */
    public seacow.Seacow.SetActiveResponse setActive(seacow.Seacow.SetActiveRequest request) {
      return blockingUnaryCall(
          getChannel(), getSetActiveMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.JoinCommonContextResponse joinCommonContext(seacow.Seacow.JoinCommonContextRequest request) {
      return blockingUnaryCall(
          getChannel(), getJoinCommonContextMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.LeaveCommonContextResponse leaveCommonContext(seacow.Seacow.LeaveCommonContextRequest request) {
      return blockingUnaryCall(
          getChannel(), getLeaveCommonContextMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.StartContextChangesResponse startContextChanges(seacow.Seacow.StartContextChangesRequest request) {
      return blockingUnaryCall(
          getChannel(), getStartContextChangesMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.EndContextChangesResponse endContextChanges(seacow.Seacow.EndContextChangesRequest request) {
      return blockingUnaryCall(
          getChannel(), getEndContextChangesMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.UndoContextChangesResponse undoContextChanges(seacow.Seacow.UndoContextChangesRequest request) {
      return blockingUnaryCall(
          getChannel(), getUndoContextChangesMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.PublishChangesDecisionResponse publishChangesDecision(seacow.Seacow.PublishChangesDecisionRequest request) {
      return blockingUnaryCall(
          getChannel(), getPublishChangesDecisionMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.SuspendParticipationResponse suspendParticipation(seacow.Seacow.SuspendParticipationRequest request) {
      return blockingUnaryCall(
          getChannel(), getSuspendParticipationMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.ResumeParticipationResponse resumeParticipation(seacow.Seacow.ResumeParticipationRequest request) {
      return blockingUnaryCall(
          getChannel(), getResumeParticipationMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.MostRecentContextCouponResponse mostRecentContextCoupon(seacow.Seacow.MostRecentContextCouponRequest request) {
      return blockingUnaryCall(
          getChannel(), getMostRecentContextCouponMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.InstanceInformationResponse instanceInformation(seacow.Seacow.InstanceInformationRequest request) {
      return blockingUnaryCall(
          getChannel(), getInstanceInformationMethod(), getCallOptions(), request);
    }

    /**
     */
    public seacow.Seacow.ImplementationInformationResponse implementationInformation(seacow.Seacow.ImplementationInformationRequest request) {
      return blockingUnaryCall(
          getChannel(), getImplementationInformationMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * ContextManager Service
   * </pre>
   */
  public static final class ContextManagerFutureStub extends io.grpc.stub.AbstractStub<ContextManagerFutureStub> {
    private ContextManagerFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextManagerFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextManagerFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextManagerFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.SetActiveResponse> setActive(
        seacow.Seacow.SetActiveRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSetActiveMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.JoinCommonContextResponse> joinCommonContext(
        seacow.Seacow.JoinCommonContextRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getJoinCommonContextMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.LeaveCommonContextResponse> leaveCommonContext(
        seacow.Seacow.LeaveCommonContextRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getLeaveCommonContextMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.StartContextChangesResponse> startContextChanges(
        seacow.Seacow.StartContextChangesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getStartContextChangesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.EndContextChangesResponse> endContextChanges(
        seacow.Seacow.EndContextChangesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getEndContextChangesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.UndoContextChangesResponse> undoContextChanges(
        seacow.Seacow.UndoContextChangesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUndoContextChangesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.PublishChangesDecisionResponse> publishChangesDecision(
        seacow.Seacow.PublishChangesDecisionRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getPublishChangesDecisionMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.SuspendParticipationResponse> suspendParticipation(
        seacow.Seacow.SuspendParticipationRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSuspendParticipationMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.ResumeParticipationResponse> resumeParticipation(
        seacow.Seacow.ResumeParticipationRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getResumeParticipationMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.MostRecentContextCouponResponse> mostRecentContextCoupon(
        seacow.Seacow.MostRecentContextCouponRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getMostRecentContextCouponMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.InstanceInformationResponse> instanceInformation(
        seacow.Seacow.InstanceInformationRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getInstanceInformationMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.ImplementationInformationResponse> implementationInformation(
        seacow.Seacow.ImplementationInformationRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getImplementationInformationMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_ACTIVE = 0;
  private static final int METHODID_JOIN_COMMON_CONTEXT = 1;
  private static final int METHODID_LEAVE_COMMON_CONTEXT = 2;
  private static final int METHODID_START_CONTEXT_CHANGES = 3;
  private static final int METHODID_END_CONTEXT_CHANGES = 4;
  private static final int METHODID_UNDO_CONTEXT_CHANGES = 5;
  private static final int METHODID_PUBLISH_CHANGES_DECISION = 6;
  private static final int METHODID_SUSPEND_PARTICIPATION = 7;
  private static final int METHODID_RESUME_PARTICIPATION = 8;
  private static final int METHODID_MOST_RECENT_CONTEXT_COUPON = 9;
  private static final int METHODID_INSTANCE_INFORMATION = 10;
  private static final int METHODID_IMPLEMENTATION_INFORMATION = 11;
  private static final int METHODID_CONTEXT_PARTICIPANT_STREAM = 12;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ContextManagerImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ContextManagerImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_ACTIVE:
          serviceImpl.setActive((seacow.Seacow.SetActiveRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.SetActiveResponse>) responseObserver);
          break;
        case METHODID_JOIN_COMMON_CONTEXT:
          serviceImpl.joinCommonContext((seacow.Seacow.JoinCommonContextRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.JoinCommonContextResponse>) responseObserver);
          break;
        case METHODID_LEAVE_COMMON_CONTEXT:
          serviceImpl.leaveCommonContext((seacow.Seacow.LeaveCommonContextRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.LeaveCommonContextResponse>) responseObserver);
          break;
        case METHODID_START_CONTEXT_CHANGES:
          serviceImpl.startContextChanges((seacow.Seacow.StartContextChangesRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.StartContextChangesResponse>) responseObserver);
          break;
        case METHODID_END_CONTEXT_CHANGES:
          serviceImpl.endContextChanges((seacow.Seacow.EndContextChangesRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.EndContextChangesResponse>) responseObserver);
          break;
        case METHODID_UNDO_CONTEXT_CHANGES:
          serviceImpl.undoContextChanges((seacow.Seacow.UndoContextChangesRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.UndoContextChangesResponse>) responseObserver);
          break;
        case METHODID_PUBLISH_CHANGES_DECISION:
          serviceImpl.publishChangesDecision((seacow.Seacow.PublishChangesDecisionRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.PublishChangesDecisionResponse>) responseObserver);
          break;
        case METHODID_SUSPEND_PARTICIPATION:
          serviceImpl.suspendParticipation((seacow.Seacow.SuspendParticipationRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.SuspendParticipationResponse>) responseObserver);
          break;
        case METHODID_RESUME_PARTICIPATION:
          serviceImpl.resumeParticipation((seacow.Seacow.ResumeParticipationRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.ResumeParticipationResponse>) responseObserver);
          break;
        case METHODID_MOST_RECENT_CONTEXT_COUPON:
          serviceImpl.mostRecentContextCoupon((seacow.Seacow.MostRecentContextCouponRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.MostRecentContextCouponResponse>) responseObserver);
          break;
        case METHODID_INSTANCE_INFORMATION:
          serviceImpl.instanceInformation((seacow.Seacow.InstanceInformationRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.InstanceInformationResponse>) responseObserver);
          break;
        case METHODID_IMPLEMENTATION_INFORMATION:
          serviceImpl.implementationInformation((seacow.Seacow.ImplementationInformationRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.ImplementationInformationResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CONTEXT_PARTICIPANT_STREAM:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.contextParticipantStream(
              (io.grpc.stub.StreamObserver<seacow.Seacow.ContextParticipantStreamResponse>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ContextManagerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ContextManagerBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return seacow.Seacow.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ContextManager");
    }
  }

  private static final class ContextManagerFileDescriptorSupplier
      extends ContextManagerBaseDescriptorSupplier {
    ContextManagerFileDescriptorSupplier() {}
  }

  private static final class ContextManagerMethodDescriptorSupplier
      extends ContextManagerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ContextManagerMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ContextManagerGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ContextManagerFileDescriptorSupplier())
              .addMethod(getSetActiveMethod())
              .addMethod(getJoinCommonContextMethod())
              .addMethod(getLeaveCommonContextMethod())
              .addMethod(getStartContextChangesMethod())
              .addMethod(getEndContextChangesMethod())
              .addMethod(getUndoContextChangesMethod())
              .addMethod(getPublishChangesDecisionMethod())
              .addMethod(getSuspendParticipationMethod())
              .addMethod(getResumeParticipationMethod())
              .addMethod(getMostRecentContextCouponMethod())
              .addMethod(getInstanceInformationMethod())
              .addMethod(getImplementationInformationMethod())
              .addMethod(getContextParticipantStreamMethod())
              .build();
        }
      }
    }
    return result;
  }
}
