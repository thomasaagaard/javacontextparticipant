package seacow;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * ContextAction Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.18.0)",
    comments = "Source: seacow.proto")
public final class ContextActionGrpc {

  private ContextActionGrpc() {}

  public static final String SERVICE_NAME = "seacow.ContextAction";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<seacow.Seacow.PerformRequest,
      seacow.Seacow.PerformResponse> getPerformMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Perform",
      requestType = seacow.Seacow.PerformRequest.class,
      responseType = seacow.Seacow.PerformResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seacow.Seacow.PerformRequest,
      seacow.Seacow.PerformResponse> getPerformMethod() {
    io.grpc.MethodDescriptor<seacow.Seacow.PerformRequest, seacow.Seacow.PerformResponse> getPerformMethod;
    if ((getPerformMethod = ContextActionGrpc.getPerformMethod) == null) {
      synchronized (ContextActionGrpc.class) {
        if ((getPerformMethod = ContextActionGrpc.getPerformMethod) == null) {
          ContextActionGrpc.getPerformMethod = getPerformMethod = 
              io.grpc.MethodDescriptor.<seacow.Seacow.PerformRequest, seacow.Seacow.PerformResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seacow.ContextAction", "Perform"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.PerformRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seacow.Seacow.PerformResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ContextActionMethodDescriptorSupplier("Perform"))
                  .build();
          }
        }
     }
     return getPerformMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ContextActionStub newStub(io.grpc.Channel channel) {
    return new ContextActionStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ContextActionBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ContextActionBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ContextActionFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ContextActionFutureStub(channel);
  }

  /**
   * <pre>
   * ContextAction Service
   * </pre>
   */
  public static abstract class ContextActionImplBase implements io.grpc.BindableService {

    /**
     */
    public void perform(seacow.Seacow.PerformRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.PerformResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getPerformMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getPerformMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seacow.Seacow.PerformRequest,
                seacow.Seacow.PerformResponse>(
                  this, METHODID_PERFORM)))
          .build();
    }
  }

  /**
   * <pre>
   * ContextAction Service
   * </pre>
   */
  public static final class ContextActionStub extends io.grpc.stub.AbstractStub<ContextActionStub> {
    private ContextActionStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextActionStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextActionStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextActionStub(channel, callOptions);
    }

    /**
     */
    public void perform(seacow.Seacow.PerformRequest request,
        io.grpc.stub.StreamObserver<seacow.Seacow.PerformResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getPerformMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * ContextAction Service
   * </pre>
   */
  public static final class ContextActionBlockingStub extends io.grpc.stub.AbstractStub<ContextActionBlockingStub> {
    private ContextActionBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextActionBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextActionBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextActionBlockingStub(channel, callOptions);
    }

    /**
     */
    public seacow.Seacow.PerformResponse perform(seacow.Seacow.PerformRequest request) {
      return blockingUnaryCall(
          getChannel(), getPerformMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * ContextAction Service
   * </pre>
   */
  public static final class ContextActionFutureStub extends io.grpc.stub.AbstractStub<ContextActionFutureStub> {
    private ContextActionFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ContextActionFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextActionFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ContextActionFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<seacow.Seacow.PerformResponse> perform(
        seacow.Seacow.PerformRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getPerformMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_PERFORM = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ContextActionImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ContextActionImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PERFORM:
          serviceImpl.perform((seacow.Seacow.PerformRequest) request,
              (io.grpc.stub.StreamObserver<seacow.Seacow.PerformResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ContextActionBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ContextActionBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return seacow.Seacow.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ContextAction");
    }
  }

  private static final class ContextActionFileDescriptorSupplier
      extends ContextActionBaseDescriptorSupplier {
    ContextActionFileDescriptorSupplier() {}
  }

  private static final class ContextActionMethodDescriptorSupplier
      extends ContextActionBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ContextActionMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ContextActionGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ContextActionFileDescriptorSupplier())
              .addMethod(getPerformMethod())
              .build();
        }
      }
    }
    return result;
  }
}
