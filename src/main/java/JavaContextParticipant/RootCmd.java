// # ContextParticipant
// This file contains a method for each example/use-case case.
//
// * For general information about gRPC see [https://grpc.io/docs/guides/](https://grpc.io/docs/guides/).
// * For information about the Java implementation of gRPC see [https://grpc.io/docs/tutorials/basic/java.html](https://grpc.io/docs/tutorials/basic/java.html).
// * For API docs on the HTTP mapping see [http://api.seacow.sirenia.eu](http://api.seacow.sirenia.eu).


package JavaContextParticipant;

import JavaContextParticipant.RequestQueue.ActualRequestQueueItem;
import JavaContextParticipant.RequestQueue.RequestQueueItem;
import JavaContextParticipant.RequestQueue.TerminalRequestQueueItem;
import io.grpc.stub.StreamObserver;
import picocli.CommandLine;
import seacow.*;

import java.util.Map;
import java.util.concurrent.LinkedTransferQueue;
import java.util.logging.Logger;

// The `RootCmd` delegates to its list of sub-commands implemented as `@CommandLine.Command` annotated methods.
@CommandLine.Command(mixinStandardHelpOptions = true)
public class RootCmd implements Runnable {

    private static final Logger logger = Logger.getLogger(RootCmd.class.getName());

    // The host of the server we'll connect to.
    @CommandLine.Option(names = {"--host"}, description = "The host to connect to", defaultValue = "localhost")
    private String host;

    // The port property determines which port we're connecting to.
    @CommandLine.Option(names = {"-p", "--port"}, description = "The port to connect to", defaultValue = "9000")
    private int port;

    // ## Registry examples

    // ### List actions
    // This service is invoked to list the actions available on the service instance which you've connected to.
    @CommandLine.Command(mixinStandardHelpOptions = true)
    public void ListActions() {

        // First, we'll need to obtain a client for the registry service.
        RegistryGrpc.RegistryBlockingStub registry = new Seacow(host, port).getRegistry();

        // Next, we'll invoke the `LocalActions` method with a very sparse request.
        seacow.Seacow.LocalActionsResponse lar = registry.localActions(seacow.Seacow.LocalActionsRequest.newBuilder().build());

        // Go through the received actions
        for (seacow.Seacow.LocalAction la : lar.getActionsList()) {
            // Log out each action
            logger.info(la.toString());
        }
    }

    // ### List items of a specific type
    // This can be used to figure out which ContextManager to connect to. Normally a Manatee will only run one instance
    // but it will spawn more if multiple applications of the same type are discovered.
    @CommandLine.Command(mixinStandardHelpOptions = true)
    public void OfType(@CommandLine.Option(names = {"--typename"}, defaultValue = "ContextManager") String typename) {
        RegistryGrpc.RegistryBlockingStub registry = new Seacow(host, port).getRegistry();

        // We invoke the `OfType` method and supply the `typename`. Currently only "ContextManager" is allowed for Manatee requests.
        seacow.Seacow.OfTypeResponse otr = registry.ofType(seacow.Seacow.OfTypeRequest.newBuilder().setTypeName(typename).build());

        // The response is a collection of identifiers (strings)
        for (String id : otr.getIdentifiersList()) {
            // For each identifier we fetch a bit more information and log out the result.
            seacow.Seacow.ComponentByIDResponse cbir = registry.componentByID(seacow.Seacow.ComponentByIDRequest.newBuilder().setIdentifier(id).build());
            logger.info(cbir.toString());
        }
    }
    // ## ContextManager examples

    // ### Join a context session
    // As a context participant you can join a session to
    //
    // - participate in context synchronization,
    // - act as a context agent to perform actions,
    // - or request another agent to perform a given action.
    //
    // When joining a session you need to first decide how to requests coming from the context manager.
    // This involves choosing the protocol on which your participant is reachable. For this example we'll use a gRPC
    // stream to allow the context manager to contact us.
    //
    // Other options include:
    //
    // - running a HTTP server and implementing the context participant,
    // - connecting via a WebSocket and responding to the JSON-RPC invocations channeled here.
    @CommandLine.Command(mixinStandardHelpOptions = true)
    public void JoinCommonContext(
            @CommandLine.Option(
                    names = {"--componentId"},
                    required = true,
                    description = "The id of the context manager to join"
            ) String componentId,
            @CommandLine.Option(
                    names = {"--applicationName"},
                    required = true,
                    description = "The name identifying the application. This should be an identifier from the CMR."
            ) String applicationName,
            @CommandLine.Option(names = {"--survey"}, defaultValue = "true")
            boolean shouldSurvey,
            @CommandLine.Option(names = {"--wait"}, defaultValue = "true")
            boolean shouldWait
    ) throws InterruptedException {
        
        // Get access to the async context manager stub
        ContextManagerGrpc.ContextManagerStub asyncStub = new Seacow(host, port).getContextManagerAsync();

        // Initialize a  queue to handle streaming responses
        final LinkedTransferQueue<RequestQueueItem> requestQueue = new LinkedTransferQueue<>();

        // Start observing the stream. Normally we'd care about the return value, but not in this example.
        final StreamObserver<seacow.Seacow.ContextParticipantStreamRequest> stream = asyncStub.contextParticipantStream(new StreamObserver<seacow.Seacow.ContextParticipantStreamResponse>() {
            @Override
            public void onNext(seacow.Seacow.ContextParticipantStreamResponse value) {
                // The server will send us a variety of messages, setup a switch to handle each type of message
                switch (value.getContentCase()) {
                    // In case we get a reply to our join request.
                    case JOIN_COMMON_CONTEXT:
                        // We simply log out the details.
                        // For a proper implementation we'd need to keep the `ParticipantCoupon` for further interactions with the context manager.
                        seacow.Seacow.JoinCommonContextResponse join = value.getJoinCommonContext();
                        logger.info("Joined: " + join.toString());
                        break;

                    // *Note* that the context manager expects a timely reply on some of these methods.
                    // If a reply is not received by the context manager it may choose to disconnect the stream.

                    // A reply for `CONTEXT_CHANGES_PENDING` is expected and we simple always provide an "accept".
                    case CONTEXT_CHANGES_PENDING:
                        // Log the incoming `CONTEXT_CHANGES_PENDING` message.
                        seacow.Seacow.ContextParticipantStreamResponse.ContextChangesPendingResponse contextChangesPending = value.getContextChangesPending();
                        logger.info("Received CONTEXT_CHANGES_PENDING: " + contextChangesPending.toString());
                        // Build an "accept" reply
                        seacow.Seacow.ContextParticipantStreamRequest.ContextChangesPendingRequest.Builder changesPendingReplyBuilder = seacow.Seacow.ContextParticipantStreamRequest.ContextChangesPendingRequest
                                .newBuilder()
                                .setContextCoupon(contextChangesPending.getContextCoupon())
                                .setDecision("accept");
                        // Add it to the queue
                        requestQueue.offer(new ActualRequestQueueItem(seacow.Seacow.ContextParticipantStreamRequest.newBuilder().setContextChangesPending(changesPendingReplyBuilder).build()));
                        break;
                    // A reply for `AGENT_CONTEXT_CHANGES_PENDING` expected as well.
                    case AGENT_CONTEXT_CHANGES_PENDING:
                        // Log then `AGENT_CONTEXT_CHANGES_PENDING` message.
                        seacow.Seacow.ContextParticipantStreamResponse.AgentContextChangesPendingResponse agentContextChangesPending = value.getAgentContextChangesPending();
                        logger.info("Received AGENT_CONTEXT_CHANGES_PENDING: " + agentContextChangesPending.toString());
                        // Build a reply with some return values
                        seacow.Seacow.ContextParticipantStreamRequest.AgentContextChangesPendingRequest.Builder agentChangesReplyBuilder = seacow.Seacow.ContextParticipantStreamRequest.AgentContextChangesPendingRequest
                                .newBuilder()
                                .setAgentCoupon(agentContextChangesPending.getAgentCoupon())
                                .setContextCoupon(agentContextChangesPending.getContextCoupon())
                                .setDecision("accept")
                                .addItemNames("MyReturnValue").addItemValues("foo");
                        requestQueue.offer(new ActualRequestQueueItem(seacow.Seacow.ContextParticipantStreamRequest.newBuilder().setAgentContextChangesPending(agentChangesReplyBuilder).build()));
                        break;
                    // We'll handle the rest of the messages by logging to stdout.
                    case CONTEXT_CHANGES_ACCEPTED:
                    case CONTEXT_CHANGES_CANCELED:
                    case COMMON_CONTEXT_TERMINATED:
                    case PARTICIPANT_NOTIFICATION:
                    default:
                        logger.info("Received: " + value.getContentCase().toString() + ". With value: " + value.toString());
                        break;
                }
            }

            @Override
            public void onError(Throwable t) {
                logger.warning("Error in stream: " + t.getMessage());
                requestQueue.offer(new TerminalRequestQueueItem(t));
            }

            @Override
            public void onCompleted() {
                requestQueue.offer(new TerminalRequestQueueItem(null));
            }
        });

        // After the stream is setup we'll send a regular JoinCommonContext invocation using the same stream.
        seacow.Seacow.JoinCommonContextRequest.Builder joinBuilder = seacow.Seacow.JoinCommonContextRequest
                .newBuilder()
                // The application name should be an id of a registered application
                .setApplicationName(applicationName)
                // The participant should be an URL and since we're using the streaming gRPC option it's scheme must be `grpc`
                .setParticipant("grpc://" + applicationName)
                // The component identifier is used to select which context manager we're joining
                .setComponentIdentifier(componentId)
                // `survey = true` indicates we're taking an active part in the context
                .setSurvey(shouldSurvey)
                // `wait = true` indicates that we're interested in waiting for any ongoing transactions to complete
                .setWait(shouldWait);

        // Send actual join context request.
        stream.onNext(seacow.Seacow.ContextParticipantStreamRequest.newBuilder().setJoinCommonContext(joinBuilder).build());

        // We setup the consumer loop which takes items from the blocking queue and ships them across the stream.
        while(true) {
            // Consume the queue of requests.
            RequestQueueItem r = requestQueue.take();
            // If we get a null value then we're done.
            if (r instanceof TerminalRequestQueueItem) break;
            seacow.Seacow.ContextParticipantStreamRequest request = ((ActualRequestQueueItem) r).getRequest();
            logger.info("Sending request: "+request.toString());
            // Ship the request on the stream.
            stream.onNext(request);
        }

        logger.info("Session is done for me");
    }

    // ### Create a new session
    //
    // Demonstrates how to create a new context session providing `tag`, `color` and whether the `fullauto` functionality
    // should be disabled. Each session is backed by a single context manager thus the terms session and context manager
    // are used interchangeably. A note on the weirdness of supplying a `componentId` to this method: It is the responsibility
    // of the ContextManager to create new sessions (and thus new ContextManagers) therefore you must supply a known session id
    // / context manager id to create a new one.
    @CommandLine.Command(mixinStandardHelpOptions = true)
    public void CreateSession(
            @CommandLine.Option(
                    names = {"--componentId"},
                    required = true,
                    description = "The id of the context manager to create the session on"
            ) String componentId,
            @CommandLine.Option(
                    names = {"--tag"},
                    required = true,
                    description = "The tag of the context manager to create - if a context manager with the given tag already exists it will be returned and no new session/context manager is created"
            ) String tag,
            @CommandLine.Option(
                    names = {"--color"},
                    description = "The color of the context manager"
            ) String color,
            @CommandLine.Option(
                    names = {"--nofullauto"},
                    defaultValue = "false",
                    description = "If `true` then the FullAutoController will not run in the created session"
            ) boolean noFullAuto
    ) {

        Seacow s = new Seacow(host, port);
        // Get the context session service
        ContextSessionGrpc.ContextSessionBlockingStub cs = s.getContextSession();

        // Construct the request passing in the parameters from the command line
        seacow.Seacow.CreateRequest createRequest = seacow.Seacow.CreateRequest.newBuilder()
                .setComponentIdentifier(componentId)
                .setTag(tag)
                .setColor(color)
                .setNoFullAuto(noFullAuto)
                .build();

        // Now issue the request to create a new session
        seacow.Seacow.CreateResponse createResponse = cs.create(createRequest);

        // Note that the session will be pruned (deleted) automatically in a short while unless a context participant
        // joins to keep it alive.
        logger.info(createResponse.toString());
    }

    // ### Read the shared context
    //
    // This example demonstrates how to read the current value of all subjects in the shared context.
    @SuppressWarnings("Duplicates")
    @CommandLine.Command(mixinStandardHelpOptions = true)
    public void ReadState(
            @CommandLine.Option(
                    names = {"--componentId"},
                    required = true,
                    description = "The id of the context manager to join"
            ) String componentId,
            @CommandLine.Option(
                    names = {"--applicationName"},
                    required = true,
                    description = "The name identifying the application. This should be an identifier from the CMR."
            ) String applicationName
    ) {
        // Get the necessary services.
        Seacow s = new Seacow(host, port);
        ContextManagerGrpc.ContextManagerBlockingStub cm = s.getContextManager();
        ContextDataGrpc.ContextDataBlockingStub cd = s.getContextData();

        // Construct and execute the join request. We'll pretend we have a HTTP server running.
        seacow.Seacow.JoinCommonContextResponse joinResponse = cm.joinCommonContext(createScammingJoinRequest(componentId, applicationName));

        // We'll need to use the participant coupon.
        long pc = joinResponse.getParticipantCoupon();

        // Lets see which subjects are in the shared context.
        seacow.Seacow.GetItemNamesResponse namesResponse = cd.getItemNames(seacow.Seacow.GetItemNamesRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                // -1 is a *secret* shortcut to MostRecentContextCoupon
                .setContextCoupon(-1)
                .build());

        // Log all found names.
        logger.info("Found "+namesResponse.getNamesCount()+" names in the shared context");
        for (String name : namesResponse.getNamesList()) {
            logger.info("Name:" +name);
        }

        // Lets see which values are then stored for these names.
        seacow.Seacow.GetItemValuesResponse values = cd.getItemValues(seacow.Seacow.GetItemValuesRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                .setContextCoupon(-1)
                .addAllItemNames(namesResponse.getNamesList())
                .build());

        // Log all names and values. The resulting list is composed as `[name1, value1, name2, value2, ..., nameN, valueN]`.
        for (String nameOrValue : values.getItemValuesList()) {
            logger.info("NameOrValue:" +nameOrValue);
        }
    }

    // ### Write to the shared context
    //
    // Writing to the shared context is slightly involved as one must go through a complete context transaction to do so.
    // The steps involved here are:
    //
    // 1. Join the common context.
    // 2. Start a context transaction.
    // 3. Specify the changes to make in the transaction.
    // 4. End the transaction and check the results.
    // 5. Publish the changes (commit or rollback).
    @SuppressWarnings("Duplicates")
    @CommandLine.Command(mixinStandardHelpOptions = true)
    public void WriteState(
            @CommandLine.Option(
                    names = {"--componentId"},
                    required = true,
                    description = "The id of the context manager to join"
            ) String componentId,
            @CommandLine.Option(
                    names = {"--applicationName"},
                    required = true,
                    description = "The name identifying the application. This should be an identifier from the CMR."
            ) String applicationName,
            @CommandLine.Option(
                    names = {"--state"},
                    description = "The new state"
            ) Map<String, String> state
    ) {
        Seacow s = new Seacow(host, port);
        ContextManagerGrpc.ContextManagerBlockingStub cm = s.getContextManager();
        ContextDataGrpc.ContextDataBlockingStub cd = s.getContextData();

        // 1. Join the common context.
        seacow.Seacow.JoinCommonContextResponse joinResponse = cm.joinCommonContext(createScammingJoinRequest(componentId, applicationName));
        long pc = joinResponse.getParticipantCoupon();

        // 2. Start the transaction.
        seacow.Seacow.StartContextChangesResponse startChangesResponse = cm.startContextChanges(seacow.Seacow.StartContextChangesRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                .setParticipantCoupon(pc)
                .build());

        // We'll need the coupon for the transaction for later.
        long cc = startChangesResponse.getContextCoupon();

        // 3. Construct and execute a request to update the shared state
        cd.setItemValues(seacow.Seacow.SetItemValuesRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                .setContextCoupon(cc)
                .setParticipantCoupon(pc)
                .addAllItemNames(state.keySet())
                .addAllItemValues(state.values())
                .build()
        );

        // 4. End the transaction.
        seacow.Seacow.EndContextChangesResponse endResponse = cm.endContextChanges(seacow.Seacow.EndContextChangesRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                .setContextCoupon(cc)
                .build());

        // Log the responses we get.
        for (String response : endResponse.getResponsesList()) {
            logger.info("Received response: "+response);
        }

        // 5. Publish changes. For this example we'll just always "accept".
        cm.publishChangesDecision(seacow.Seacow.PublishChangesDecisionRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                .setContextCoupon(cc)
                .setDecision("accept")
                .build());
    }

    // ## Context Action example

    // ### Perform an action
    //
    // You can request an action to be performed by the context manager. It will then route the request to the appropriate
    // action agent and return the result.
    // For this example we'll use the blocking versions of the services and we'll cheat a bit by using the HTTP mapping,
    // but wont setup an actual HTTP server to respond to the context managers requests. This means that we'll get thrown off
    // the manager after a little while.
    @CommandLine.Command(mixinStandardHelpOptions = true)
    public void PerformAction(
            @CommandLine.Option(
                    names = {"--componentId"},
                    required = true,
                    description = "The id of the context manager to join"
            ) String componentId,
            @CommandLine.Option(
                    names = {"--applicationName"},
                    required = true,
                    description = "The name identifying the application. This should be an identifier from the CMR."
            ) String applicationName,
            @CommandLine.Option(
                    names = {"--action"},
                    description = "The id of the action to perform",
                    required = true
            ) String action,
            @CommandLine.Option(
                    names = {"--inputs"},
                    description = "The inputs to provide to the action"
            )
            Map<String, String> inputs
            ) {

        Seacow s = new Seacow(host, port);
        // Get a context manager service to be able to join the common context.
        ContextManagerGrpc.ContextManagerBlockingStub cm = s.getContextManager();
        // Get a context action service to be able to perform actions.
        ContextActionGrpc.ContextActionBlockingStub ca = s.getContextAction();

        // Pretend we have a HTTP server running.
        seacow.Seacow.JoinCommonContextRequest joinRequest = createScammingJoinRequest(componentId, applicationName);

        // Join the common context. Only context participants are allowed to request actions to be performed.
        seacow.Seacow.JoinCommonContextResponse joinResponse = cm.joinCommonContext(joinRequest);
        logger.info("Joined with: "+joinResponse.toString());

        // We'll need the participant coupon when asking the context manager to perform an action.
        long pc = joinResponse.getParticipantCoupon();

        // Construct the request to have an action performed.
        seacow.Seacow.PerformRequest.Builder performRequestBuilder = seacow.Seacow.PerformRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                .setParticipantCoupon(pc)
                .setActionIdentifier(action);

        // If the action needs an input to run we can also provide these in the request.
        if (inputs != null && inputs.size() > 0) {
            // Iterate the provided inputs and add to request.
            for (String name : inputs.keySet()) {
                performRequestBuilder.addInputNames(name);
                performRequestBuilder.addInputValues(inputs.get(name));
            }
        }

        // Execute the request.
        seacow.Seacow.PerformResponse performResponse = ca.perform(performRequestBuilder.build());
        logger.info("Received a perform response: " + performResponse.toString());

        // We really need to execute a LeaveCommonContext but the context manager will figure out we're gone after a while
        // so for this example we'll be lazy.
    }

    // ## Utility functions

    // Create "scamming" join request. The scam is that we're not really running a context participant at `localhost:1234`.
    private seacow.Seacow.JoinCommonContextRequest createScammingJoinRequest(@CommandLine.Option(names = {"--componentId"}, required = true, description = "The id of the context manager to join") String componentId, @CommandLine.Option(names = {"--applicationName"}, required = true, description = "The name identifying the application. This should be an identifier from the CMR.") String applicationName) {
        return seacow.Seacow.JoinCommonContextRequest
                .newBuilder()
                .setComponentIdentifier(componentId)
                .setApplicationName(applicationName)
                .setWait(true)
                .setSurvey(true)
                // This endpoint is a scam.
                .setParticipant("http://localhost:1234")
                .build();
    }

    @Override
    public void run() {
        // Show help message if no sub-command is given
        CommandLine.usage(new RootCmd(), System.out);
    }
}
