package JavaContextParticipant.RequestQueue;

import seacow.Seacow;

public class ActualRequestQueueItem implements RequestQueueItem {
    private Seacow.ContextParticipantStreamRequest request;

    public ActualRequestQueueItem(Seacow.ContextParticipantStreamRequest r) {
        request = r;
    }

    public seacow.Seacow.ContextParticipantStreamRequest getRequest() {
        return request;
    }
}
