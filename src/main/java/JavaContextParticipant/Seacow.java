// # Seacow
// The Seacow class is a simple wrapper for the gRPC channel and clients for the gRPC services defined in the seacow.proto file.

package JavaContextParticipant;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import seacow.*;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Seacow {

    private static final Logger logger = Logger.getLogger(Seacow.class.getName());

    private final ContextManagerGrpc.ContextManagerStub cmAsync;
    private final ContextManagerGrpc.ContextManagerBlockingStub cm;
    private final ContextDataGrpc.ContextDataBlockingStub cd;
    private final ContextActionGrpc.ContextActionBlockingStub ca;
    private final ContextSessionGrpc.ContextSessionBlockingStub cs;
    private final RegistryGrpc.RegistryBlockingStub registry;

    private final ManagedChannel channel;


    public Seacow() {
        // When connecting without giving a `host` or `port` we'll use `localhost:9000`
        this("localhost", 9000);
    }

    public Seacow (String host, int port) {
        // When connecting we need a `host` and a `port` to connect to
        ManagedChannelBuilder<?> builder = ManagedChannelBuilder.forAddress(host, port).usePlaintext();
        channel = builder.build();

        // Create blocking stubs for services required for example uses.
        // The context manager service (non-streaming) - it implements the Context Manager interface.
        cm = seacow.ContextManagerGrpc.newBlockingStub(channel);

        // Create a blocking stub for the context data service.
        cd = seacow.ContextDataGrpc.newBlockingStub(channel);

        // Create a blocking stub for the context action service.
        ca = ContextActionGrpc.newBlockingStub(channel);

        // Create a blocking stub for the context session service.
        cs = ContextSessionGrpc.newBlockingStub(channel);

        // The registry contains convenience methods for accessing the Context Management Registry (CMR) through Manatetee.
        // The CMR can also be accessed directly, but this is rarely needed for Manatee participants.
        registry = seacow.RegistryGrpc.newBlockingStub(channel);

        // Create a non-blocking (async) stub for the context manager service. This is used for gRPC bi-directional streaming.
        cmAsync = seacow.ContextManagerGrpc.newStub(channel);
    }

    public ContextManagerGrpc.ContextManagerBlockingStub getContextManager() {
        return cm;
    }

    public ContextSessionGrpc.ContextSessionBlockingStub getContextSession() {
        return cs;
    }

    public RegistryGrpc.RegistryBlockingStub getRegistry() {
        return registry;
    }

    public ContextManagerGrpc.ContextManagerStub getContextManagerAsync() { return cmAsync; }

    public ContextDataGrpc.ContextDataBlockingStub getContextData() { return cd; }

    public ContextActionGrpc.ContextActionBlockingStub getContextAction() { return ca; }

    public void Disconnect() throws InterruptedException {
        if (channel != null) {
            // Disconnect cleanly with a 5 second timeout
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
    }


}
