// # App
package JavaContextParticipant;

import picocli.CommandLine;

// The entry-point for running the various examples
public class App {

    public static void main(String[] args) {
        // The RootCmd will kick of the show
        CommandLine.run(new RootCmd(), args);
    }
}
