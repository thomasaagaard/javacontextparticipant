This repo contains the code for a Java ContextParticipant.

## Build it

Downloads dependencies, generates gRPC stubs and compiles it all.

    > ./gradlew build

### Generate a jar

    > ./gradlew jar

## Run it

    > java -jar build/libs/JavaContextParticipant.jar --help

Will display helpful information and further info on how to run the participant.

## Generate documentation

Requires [docco](https://github.com/jashkenas/docco). 

    > docco -o docs src/main/java/JavaContextParticipant/*.java

Or run the `docs.sh` shell-script (on Linux/BSD/OSX).
